# OpenML dataset: Meta_Album_FLW_Mini

https://www.openml.org/d/44283

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Flowers Dataset (Mini)**
***
The Flowers dataset(https://www.robots.ox.ac.uk/~vgg/data/flowers/102/index.html) consists of a variety of flowers gathered from different websites and some are photographed by the original creators. These flowers are commonly found in the UK. The images generally have large scale, pose and light variations. Some categories of flowers in the dataset has large variations of flowers while other have similar flowers in a category. The dataset was created back in 2008 at Oxford University by Nilsback, M-E. and Zisserman, A.[21]. The Flowers dataset in the Meta-Album meta-dataset is a preprocessed version of the original flowers dataset. The images are first cropped and made into squared images which are then resized into 128x128 with anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/FLW.png)

**Meta Album ID**: PLT.FLW  
**Meta Album URL**: [https://meta-album.github.io/datasets/FLW.html](https://meta-album.github.io/datasets/FLW.html)  
**Domain ID**: PLT  
**Domain Name**: Plants  
**Dataset ID**: FLW  
**Dataset Name**: Flowers  
**Short Description**: Flowers dataset from Visual Geometry Group, University of Oxford  
**\# Classes**: 102  
**\# Images**: 4080  
**Keywords**: flowers, ecology, plants  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: GNU General Public License Version 2  
**License URL(original data release)**: https://peltarion.com/knowledge-center/documentation/terms/dataset-licenses/oxford-102-category-flowers
https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
 
**License (Meta-Album data release)**: GNU General Public License Version 2  
**License URL (Meta-Album data release)**: [https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)  

**Source**: Visual Geometry Group, University of Oxford, England  
**Source URL**: https://www.robots.ox.ac.uk/~vgg/data/flowers/  
  
**Original Author**: Maria-Elena Nilsback,  Andrew Zisserman  
**Original contact**: {men,az}@robots.ox.ac.uk  

**Meta Album author**: Felix Mohr  
**Created Date**: 01 March 2022  
**Contact Name**: Felix Mohr  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@InProceedings{Nilsback08,
  author       = "Maria-Elena Nilsback and Andrew Zisserman",
  title        = "Automated Flower Classification over a Large Number of Classes",
  booktitle    = "Indian Conference on Computer Vision, Graphics and Image Processing",
  month        = "Dec",
  year         = "2008",
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44239)  [[Extended]](https://www.openml.org/d/44318)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44283) of an [OpenML dataset](https://www.openml.org/d/44283). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44283/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44283/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44283/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

